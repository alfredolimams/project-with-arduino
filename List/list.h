#ifndef LIST
#define LIST

#include "point.h"


class List {
    int scale;
    Point * head;
    Point * tail;
public:
    List()
    {
        head = NULL;
        tail = NULL;
        scale = 0;
    }
    void add( Point *p )
    {
        if( scale )
        {
            p->setNext(head);
            head->setBefore(p);
            head = p;
        }
        else
            head = tail = p;
        ++scale;
    }
    Point * at( int idx )
    {
        if( idx >= scale )
            return NULL;
        Point *p = head;
        for (int i = 0; i < idx ; ++i)
            p = p->getNext();
        return p;
    }
    void remove( int idx )
    {
        if( idx >= scale )
            return;
        if( idx == 0 )
        {
            head = head->getNext();
            delete( head->getBefore() );
            head->setBefore(NULL);
        }
        else if( idx+1 == scale )
        {
            tail = tail->getBefore();
            delete( tail->getNext() );
            tail->setNext(NULL);
        }
        else
        {
            Point *p = at(idx);
            Point *b = p->getBefore();
            Point *n = p->getNext();
            b->setNext(n);
            n->setBefore(n);
            delete(p);
        }
        --scale;
    }
    int size()
    {
        return scale;
    }
};



#endif // LIST
