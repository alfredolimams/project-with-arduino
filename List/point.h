#ifndef POINT
#define POINT

class Point{
    Point * before;
    Point * next;
public:
    int x;
    int y;
    Point()
    {
    }
    Point( int x , int y ) 
    {
        setX(x);
        setY(y);
        next = NULL;
        before = NULL;
    }
    int getX() const 
    {
        return x;
    }
    void setX(int x) 
    {
        this->x = x;
    }
    int getY() const 
    {
        return y;
    }
    void setY(int y) 
    {
        this->y = y;
    }
    Point * getNext() const 
    {
        return next;
    }
    void setNext(Point *p) 
    {
        next = p;
    }
    Point * getBefore() const 
    {
        return before;
    }
    void setBefore(Point *p) 
    {
        before = p;
    }
};

#endif // POINT

