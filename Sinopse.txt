/*
			Projeto com Arduino para laboratório de programação.

		Equipe: Alfredo L!ma, Eric Cerqueira e Matheus Inácio.


		Iremos utilizar uma matriz de LED (8x8) para fazer um jogo de Space Invaders (Atari) na plataforma de harware livre Arduino.
		Teremos o player o qual irá enfrentar a outra nave controlada pela maquina randomicamente. Cada nave terá uma certa quantidade de vida e os movimentos serão esquerda, direita e atirar sendo o tiro automatizado.

		
*/