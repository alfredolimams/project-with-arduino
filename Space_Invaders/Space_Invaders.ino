#include <LiquidCrystal.h>
#include <pcf8574.h>
#include <list.h>
PCF8574 line(0x20); //Define o endereço (0x20) responsável pelas linhas 
PCF8574 collumn(0x21); //Define o endereço (0x21) responsável pelas colunas
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
#define limit 8 // Define a matriz
#define velocity 50 // Define a velocidade em ms
#define up 0
#define down 7
#define lifes 10
#define DEBUG if(0)
#define left 8
#define right 9
#define gun 7
typedef struct players
{
  int life;
  int x;
  int y;
};

void set_player( struct players * p, int life , int x, int y )
{
  p->life = life;
  p->x = x;
  p->y = y;
}
void move_right( struct players * p )
{
  if( p->y == down )
  {
    if( p->x > 1 )
      --p->x;
  }
  else
  {
    if( p->x < 6 )
      ++p->x;
  }
}

void move_left( struct players * p )
{
  if( p->y == down )
  {
    if( p->x < 6 )
      ++p->x;
  }
  else
  {
    if( p->x > 1 )
      --p->x;
  }
}

byte * allocate_matrix()
{
  byte * matrix;
  matrix = (byte *) malloc( limit * sizeof(byte));
  memset( matrix , 0 , limit*sizeof(byte) );
  return matrix;
}

players * allocate_player()
{
  players * player;
  player = (players *) malloc( sizeof(players) );
  return player;
}

List * allocate_list()
{
  List * list;
  list = (List *) malloc( sizeof(List) );
  *list = List();
  return list;
}

void show( byte * matrix )
{
  for( int j = 0 ; j < limit ; ++j ) // Inicia contador das colunas
  {        
    collumn.write8( 1<<(j) ); //Posiciona a coluna
    line.write8( matrix[j] ); //Carrega o valor da matriz na coluna
    line.write8( 0 ); // Limpa a interface
  }
}

void joint( byte * player1, byte * player2 , byte * shot1, byte * shot2, byte * matrix )
{
  for (int i = 0; i < limit ; ++i)
    matrix[i] = ( (player1[i] | player2[i]) 
              |   ( shot1[i] | shot2[i] ) ); 
}
 
void draw_ships( byte * m1, byte *m2, struct players *p1, struct players *p2 )
{
    memset( m1, 0 , limit*sizeof(byte) );
    memset( m2, 0 , limit*sizeof(byte) );
    m1[ p1->x ] = 1<<(p1->y);
    m1[ p1->x+1 ] = 1<<(p1->y);
    m1[ p1->x-1 ] = 1<<(p1->y);
    m1[ p1->x ] |= 1<<(p1->y+1);
    m2[ p2->x ] = 1<<(p2->y);
    m2[ p2->x+1 ] = 1<<(p2->y);
    m2[ p2->x-1 ] = 1<<(p2->y);
    m2[ p2->x ] |= 1<<(p2->y-1);
}

void draw_shots( byte * m1, byte *m2, List *s1, List *s2 )
{
    memset( m1, 0 , limit*sizeof(byte) );
    memset( m2, 0 , limit*sizeof(byte) );
    for( int i = 0 ; i < s1->size() ; ++i )
    {
        Point * l1 = s1->at(i);
        m1[ l1->x ] |= 1<<(l1->y); 
    }
    for( int i = 0 ; i < s2->size() ; ++i )
    {
        Point * l2 = s2->at(i);
        m2[ l2->x ] |= 1<<(l2->y); 
    }
}

void crash( List * s , byte * m , struct players *pl )
{
  Point *p;
  for( int i = 0 ; i < s->size() ; ++i )
  {
    p = s->at(i);
    if( m[ p->x ] & 1<<( p->y ) )
    {
      --pl->life;
      s->remove(i);
      --i;    
    }
  }
}

void drive_shots( List * s1 , List * s2 )
{
  Point * p;
  for( int i = 0 ; i < s1->size() ; ++i )
  {
    p = s1->at(i);
    if( p->y == down )
    {
      s1->remove(i);
      --i;
    }
    else
      ++p->y;
  }
  for( int i = 0 ; i < s2->size() ; ++i )
  {
    p = s2->at(i);
    if( p->y == up )
    {
      s2->remove(i);
      --i;
    }
    else
      --p->y;
  } 
}

void action( struct players *p1, struct players *p2 , List * s1 , List * s2 )
{
  int moves = random( 6 );
  if( moves >= 3 )
  {
    if( s1->size() < 2 ) 
      s1->add( new Point( p1->x , 1 ) );
    moves -= 3;
  }
  if( moves >= 2 )
  {
    move_right(p1);
    moves -= 2;
  }
  if( moves )
    move_left(p1);

  if( digitalRead( left ) ^ digitalRead( right ) )
  {
    if( !digitalRead( left ) )
      move_left(p2);
    else
      move_right(p2);
  }
  if( !digitalRead(gun) && s2->size() < 2 )
    s2->add( new Point( p2->x , 6 ) );
  
}

void init(int i)
{
  lcd.clear();
  lcd.setCursor(1,0);
  lcd.print("Space Invaders");
  lcd.setCursor(0,1);
  lcd.print(" Comecando em ");
  lcd.print(i);
  delay(1000);
}

void setup() 
{
  pinMode( left , INPUT );
  pinMode( gun , INPUT );
  pinMode( right , INPUT );
  Serial.begin(9600);
  lcd.begin(16, 2);
}
void scoreboard( int p1, int p2 )
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("     PLACAR       ");
  lcd.setCursor(0,1);
  lcd.print(" J1: ");
  lcd.print(p1);
  lcd.print("   J2: ");
  lcd.print(p2);
}
void finally( int p1 )
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("     PARABENS  ");
  lcd.setCursor(0,1);
  if( p1 > 0 )
    lcd.print("     PLAYER1  ");
  else
    lcd.print("     PLAYER2  ");
  delay( 2000 );
}
void loop() 
{ 
  // Variaveis
  int v = 0;
  byte *matrix;
  byte *matrix_player1;
  byte *matrix_player2;
  byte *matrix_player_shot1;
  byte *matrix_player_shot2;
  List *shot1;
  List *shot2;
  struct players *player1;
  struct players *player2;
  // Locação de memoria
  matrix = allocate_matrix();
  matrix_player1 = allocate_matrix();
  matrix_player2 = allocate_matrix();
  matrix_player_shot1 = allocate_matrix();
  matrix_player_shot2 = allocate_matrix();
  player1 = allocate_player();
  set_player( player1 , lifes, 1 , up );
  player2 = allocate_player();
  set_player( player2 , lifes, 6 , down );
  shot1 = allocate_list();
  shot2 = allocate_list();
  for( int i = 3 ; i > 0 ; --i )
    init( i );
  // Rodar o jogo
  while( player1->life > 0 and player2->life > 0 ) 
  {
    scoreboard( (*player1).life , (*player2).life );
    Serial.println("Funfando");
    ++v;
    if( v == velocity )
    {
      // movimentos
      action( player1, player2 , shot1, shot2 );
      drive_shots( shot1, shot2 );
      // Colisões
      crash( shot1, matrix_player2, player2 );
      crash( shot2, matrix_player1, player1 );
      //
      v = 0;
    }
    // Desenhar as naves
    draw_ships( matrix_player1 , matrix_player2 , player1 , player2 );
    // Desenhar os tiros
    draw_shots( matrix_player_shot1 , matrix_player_shot2 , shot1 , shot2 );
    // Juntar os desenhos
    joint( matrix_player1 , matrix_player2 , matrix_player_shot1 , matrix_player_shot2 , matrix );
    // Mostrar na interface
    show(matrix);
  }
  memset( matrix, 0 , 8*sizeof(int) );
  show(matrix);
  finally( player1->life );
  // Liberação de memoria
  free(matrix);
  free(matrix_player1);
  free(matrix_player2);
  free(matrix_player_shot1);
  free(matrix_player_shot2);
  free(player1);
  free(player2);
  free(shot1);
  free(shot2);
  //
}


